


## I attempted another solution with simplier workflow, ES6 syntax and alt.js Flux implementation:
###https://bitbucket.org/alsekow/sainsbury-part1-react-with-altjs





##Workflow:
 * using yeoman with react-reflux generator for scaffolding
 * gulp for tasks
 * jest tests


##Features:
 * react, react router, reflux
 * compass / sass styles
 * used $.ajax only for the AJAX call
 * used reflux async actions with promises instead of provided snippet. More in source.
 * simple local storage service to persist toggle data
 * the app is restricted to pull public photos with tag 'javsacript' for easier toggle testing


To run:

 * npm install

 * gulp serve


Test:

 * use 'gulp test' to run tests with jest. There are some test stubs and some local storage tests.




####Author: Aleksander Sekowski

aleksander@uixlimited.com