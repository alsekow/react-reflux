jest
  .dontMock('../../app/scripts/services/LocalStorage');

describe("Grid", function() {
  var storage = require('../../app/scripts/services/LocalStorage');


  beforeEach(function() {
    var mock = (function() { // mocking a local storage browser API
      var store = {};
      return {
        getItem: function(key) {
          return store[key];
        },
        setItem: function(key, value) {
          store[key] = value.toString();
        },
        clear: function() {
          store = {};
        }
      };
    })();
    Object.defineProperty(window, 'localStorage', { value: mock });
  });

  it('should save and read a string', function() {
    storage.set('testString', 'foo');
    expect(storage.get('testString')).toBe('foo');
  });

  it('should save and read an array', function() {
    storage.set('testString', ['a', 'b', 'c']);
    expect(storage.get('testString').join()).toBe(['a', 'b', 'c'].join());
  });


});
