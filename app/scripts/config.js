
const config = {
  API_PATH: 'https://api.flickr.com/services/feeds/photos_public.gne?',
  API_PARAMS: 'format=json'
};


module.exports = config;
