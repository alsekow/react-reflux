var React = require('react');
var Reflux = require('reflux');

var GridStore = require('../stores/GridStore');
var GridActions = require('../actions/GridActions');
var GridCell = require('./gridCell.jsx');


// Grid is a smart component. It knows about the store and the actions
var Grid = React.createClass({

  mixins: [Reflux.connect(GridStore)],


  componentDidMount() {
    GridActions.loadList();
  },


  toggleCell(item) {
    GridActions.toggleItem(item);
  },


  renderCell(item, i) {
    return (
      <GridCell
        data={item}
        onClick={this.toggleCell.bind(this, item)}  />
    )
  },


  render() {
    return (
      <div>
        {this.state.list && this.state.list.length > 0 ? this.state.list.map(this.renderCell) : 'Loading...'}
      </div>
    );
  }

});


module.exports = Grid;
