var React = require('react');

var Grid = require('./grid');

var Home = React.createClass({

  render() {
    return (
      <div>
        <Grid />
      </div>
    );
  }
});

module.exports = Home;
