var React = require('react');


// This is a 'dumb' component. It only communicates with grid using props. Doesn't know about stores or actions
var GridCell = React.createClass({

  render: function() {
    return (
      <div className="cell">
        <img src={this.props.data.media.m}
          alt="this.props.data.title"
          onClick={this.props.onClick}
          className={this.props.data.toggled ? 'selected' : ''}/>
      </div>
    );
  }

});

module.exports = GridCell;
