
// A simple local storage service for reading / posting state values
function LocalStorage() {
  this.set = function(path, value) {
    localStorage.setItem(path, JSON.stringify(value));
  };

  this.get = function(path) {
    var value = localStorage.getItem(path);
    return value ? JSON.parse(localStorage.getItem(path)) : false;
  };

};


module.exports = new LocalStorage();
