var Reflux = require('reflux');

var GridActions = require('../actions/GridActions');
var LocalStorageService = require('../services/LocalStorage');



// Our store takes care of all data needs
var GridStore = Reflux.createStore({
  listenables: [GridActions],


  //this is our local storage list
  localList: LocalStorageService.get('grid_list') || [],


  getInitialState() {
    return {
      list: []
    }
  },


  // persist the state to local storage to allow for saving toggles
  updateLocalList(list) {
    LocalStorageService.set('grid_list', list);
  },


  // when loading data starts
  loadList() {
    this.trigger({
      error: 'Data is downloading...'
    });
  },


  // when loading data succeeds
  loadListCompleted(res) {
    this.list = res.items;
    this.diffListWithLocal(this.list, this.localList); // before displaying check for toggle flags

    this.trigger({
      list: res.items
    });
  },


  // when loading data fails
  loadListFailed(res) {
    this.trigger({
      error: 'Data could not be downloaded'
    });
  },


  // Check if any picture on new list from API has been toggled before
  diffListWithLocal(stateList, localList) {
    stateList.map((d, i) => {
      var match = localList.filter((k, n) => {
        if (k.media.m == d.media.m) // mathichng is based un image url, which is unique
          return true;
      });

      if (match.length > 0 && match[0].toggled)
        d.toggled = match[0].toggled; // update toggle state base d on local storage values
    });

    this.localList = stateList; // update local list
  },


  toggleItem(item) {
    var toggledItem = this.list.map((d, i) => {
      if (d.media.m == item.media.m) {
        if (item.toggled)
          d.toggled = !item.toggled;
        else
          d.toggled = true;
      }
    });

    this.updateLocalList(this.list); // store our toggle in local storage as well
    this.trigger({
      list: this.list
    });
  }

});


module.exports = GridStore;
