var Reflux = require('reflux');
var $ = require('jquery'); // I realize jquery has been discouraged, but it is only used as an AJAX interface here

var config = require('../config');


var apiCall = function() {
  // using a simple ajax call with jsonp callback, as it's easier to return promise and send that to the store
  return $.ajax({ // returns the promise
    url: config.API_PATH + config.API_PARAMS,
    dataType: 'jsonp',
    jsonpCallback: 'jsonFlickrFeed',
    cache: false,
    context: this
  });
};


var GridActions = Reflux.createActions({
  loadList: {children: ["completed","failed"]},
  toggleItem: {}
});


GridActions.loadList.listen(function() { // This way we take care of handling our Async action
  apiCall()
    .done(this.completed)
    .fail(this.failed);
});


module.exports = GridActions;
